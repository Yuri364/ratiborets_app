var args = arguments[0] || {};

var lib = require('global');
var win = $.activities;
var title = "Мероприятия";
lib.action_bar(win,title);

var Parse = require('/Parse');

var top = ['Текущие','Прошедшие'];

var font_family = 'pfdintextcondpro-regular'; //fontFamily

var viewTop = Titanium.UI.createView({
	top: 0,
	left: 0,
	height: 50,
	backgroundColor: '#3b3b3b',
	width: 'auto',
	layout:'horizontal',
});

var days_bg = '#3b3b3b';
var view_bg = 'white';

for (var i = 0; i < top.length; i++) {
	
	var viewLabelBg = Titanium.UI.createView({
		   top: 0,
		   width: '50%',
		   height: 50,
		   backgroundColor: i==0?'#fff':'#3b3b3b',
		   viewId: i+1,
		   touchEnabled: true
	});
	
	var label = Titanium.UI.createLabel({
	   	   font:{fontSize:19,fontFamily:font_family},
		   text: top[i],
		   textAlign: 'center',
		   color: i==0?'#3b3b3b':'#fff',
		   touchEnabled: false
	 });
	 viewLabelBg.add(label);
	 viewTop.add(viewLabelBg);
}

function get_activies(period) {
	
	var date = new Date();
	var date_iso = date.toISOString();
	
	//console.log(date_iso);
	
	if(period == 1) {
		var wh = 'where={"public":"1","date":{"$gte":{"__type":"Date","iso":"'+date_iso+'"}}}';
	}
	else {
		var wh = 'where={"public":"1","date":{"$lte":{"__type":"Date","iso":"'+date_iso+'"}}}';
	}
	
	Parse.getObjects('activities?order=-date&'+wh, function(res,data,status) {
		
		//console.log(res);
		
		if(res == 1) {
			var tableData = [];
			data = JSON.parse(data);
			data = data.results;
			
			
			if(data.length>0) {
			for (var i = 0; i < data.length; i++) {
				var act = data[i];
				
				var row = Ti.UI.createTableViewRow({
					selectedBackgroundColor:'white',
					rowIndex:act.objectId,
					backgroundColor: '#fff',
					height:"auto",
				});
				
				var view = Ti.UI.createView({
					left:160, 
					right: 5,
					bottom: 5,
					rowIndex:act.objectId,
					height:"auto",
					layout:'vertical'
			 	});
				
				var labelTitle = Ti.UI.createLabel({
					color:'#2a2a2a',
					font:{fontSize:19,fontFamily:font_family},
					text: act.title,
					top: 10,
					left: 10,
					rowIndex:act.objectId,
					height:"auto"
			 	});
				view.add(labelTitle);

				var labelDesc = Ti.UI.createLabel({
					color:'#2a2a2a',
					font:{fontSize:16,fontFamily:font_family},
					text: act.desc,
					top: 5,
					left: 10,
					rowIndex:act.objectId,
					height:"auto"
			 	});
				view.add(labelDesc);
				
				var labelCalendar = Ti.UI.createLabel({
					color:'#f00',
					font:{
						fontSize:18,
						fontFamily: "Ionicons",
					},
					text: "\uf117",
					left:10,
					top: 5,
					rowIndex:act.objectId,
					height:"auto"
				});
				view.add(labelCalendar);
				
				var d = lib.normalDate(act.date,1);
				
				var labelDate = Ti.UI.createLabel({
					text: d['date'],
					color: '#f00',
					font:{fontSize:18,fontFamily:font_family},
					left: 30,
					top: -20,
					rowIndex:act.objectId,
				});
				view.add(labelDate);
				
				var labelTimeIcon = Ti.UI.createLabel({
					color:'red',
					font:{fontSize:18,fontFamily: "Ionicons"},
					text: "\uf26e",
					top: 5,
					left:10,
					height:"auto"
				});
				view.add(labelTimeIcon);
				
				var labelTime = Ti.UI.createLabel({
					text: act.time+' ч.',
					color: '#f00',
					font:{fontSize:18,fontFamily:font_family},
					left: 30,
					top: -20,
					rowIndex:act.objectId,
				});
				view.add(labelTime);
				
				var imageView = Ti.UI.createImageView({
					left:10, 
					top:10,
					width: 150,
					height: 210,
					bottom: 10,
					image: act.photo.url,
					rowIndex:act.objectId,
				});
				
				row.add(view);
				row.add(imageView);
				
				tableData.push(row);
			}
			}
			else {
				var row = Ti.UI.createTableViewRow({
					selectedBackgroundColor:'white',
					backgroundColor: '#fff',
					height:"auto",
				});
				var textLabel = Ti.UI.createLabel({
					color:'#2a2a2a',
					font:{fontSize:19,fontFamily:font_family},
					text: 'Мероприятия не найдены...',
					top: 25,
					height:"auto"
			 	});
				row.add(textLabel);
				tableData.push(row);
			}
			
			var tableView = Ti.UI.createTableView({
				backgroundColor:'white',
				separatorColor: '#f1f1f1',
				data:tableData,
				top: 50,
				//separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,//ios
			});
			
			win.add(tableView);
		}
	});
}
viewTop.addEventListener('click', function(e){ 
	
	 if(win.children[2] != undefined) {
	 	win.remove(win.children[2]);//remove table
	 }
	
	var viewId = e.source.viewId;
	
	e.source.applyProperties({
        backgroundColor: view_bg
    });
	e.source.children[0].applyProperties({
        color: days_bg
    });
    
    get_activies(viewId);
    
    var childrenView = viewTop.children;
    
    for (var i = 0; i < childrenView.length; i++) {
    	
    	var childViewId = childrenView[i].viewId;
    	
    	if(childViewId != viewId) {
    		childrenView[i].applyProperties({
		        backgroundColor: days_bg
		    });
    		childrenView[i].children[0].applyProperties({
		        color: view_bg
		    });
    	}
    }
});


get_activies(1);


win.add(viewTop);