var args = arguments[0] || {};

var lib = require('global');
var win = $.contacts;
var title = "Контакты";


lib.action_bar(win,title);

function doCall() {
	//android
	var the_number = '+73432861139';
	Ti.Platform.openURL('tel:'+the_number);
}

function doMail() {
	//android
	var the_mail = 'pr@ratiborets.ru';
	Ti.Platform.openURL('mailto:'+the_mail);
}

function doSocial(e) {
	var rowId = e.source.rowId;
	if(rowId == 1) {
		Titanium.Platform.openURL('http://vk.com/ratiborets_ekb');
	}
	else if(rowId == 2) {
		Titanium.Platform.openURL('https://www.facebook.com/ratiborets.ekb');
	}
	else if(rowId == 3) {
		Titanium.Platform.openURL('https://instagram.com/ratiborets');
	}
}