var args = arguments[0] || {};

var lib = require('global');

var win = $.full_post;
var font_family = 'pfdintextcondpro-regular'; //fontFamily

lib.action_bar(win,args.title);

var text = Ti.UI.createLabel({
	color:'#2a2a2a',
	font:{
		fontSize: 19,
		fontFamily: "pfdintextcondpro-regular",
	},
	text: args.text,
	left:10, 
	top: 10,
	right: 10,
	height:"auto"
});

var scrollView = Ti.UI.createScrollView({
	showVerticalScrollIndicator: true,
	showHorizontalScrollIndicator: true,
	height: 'auto',
	width: 'auto'
});

var viewBg = Titanium.UI.createView({
	top: 0,
	left: 0,
	right: 0,
	bottom: 0,
	height: "auto",
	widht: "auto",
	backgroundColor: "#fff",
});

var imageView = Ti.UI.createImageView({
	image: args.photo.url,
	left:10, 
	top:10,
	right: 10,
	backgroundColor: "#ccc",
});

viewBg.add(text);
scrollView.add(viewBg);
win.add(scrollView);