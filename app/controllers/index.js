var Cloud = require("ti.cloud");

//Android push notification
if(OS_ANDROID) {
	
	var CloudPush = require('ti.cloudpush');
	var deviceToken = null;
	
	// Initialize the module
	CloudPush.retrieveDeviceToken({
	    success: deviceTokenSuccess,
	    error: deviceTokenError
	});
	
	// Enable push notifications for this device
	// Save the device token for subsequent API calls
	function deviceTokenSuccess(e) {
	    deviceToken = e.deviceToken;
	    //Подписываем на канал по умолчанию
	    subscribeToChannel(deviceToken);
	}
	
	function deviceTokenError(e) {
	    console.log('Failed to register for push notifications! ' + e.error);
	}
	// Process incoming push notifications
	CloudPush.addEventListener('callback', function (evt) {
	    console.log("Notification received: " + evt.payload);
	});
}

function subscribeToChannel(deviceToken) {
    Cloud.PushNotifications.subscribeToken({
        device_token: deviceToken,
        channel: 'news_alerts',
        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
    }, function (e) {
 	if (e.success) {
            console.log('Subscribed');
        } else {
            console.log('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
   });
}

function unsubscribeToChannel (deviceToken) {
    // Unsubscribes the device from the 'test' channel
    Cloud.PushNotifications.unsubscribeToken({
        device_token: deviceToken,
        channel: 'news_alerts',
    }, function (e) {
        if (e.success) {
            alert('Unsubscribed');
        } else {
            alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
}

function onMenuButtonClick(e){
    $.index.toggleLeftView();
}

function onDrawerOpen(e) {
    Ti.API.info($.index.isLeftDrawerOpen);
}

function onDrawerClose(e) {
    Ti.API.info($.index.isLeftDrawerOpen);
}


$.menuC.on('menuclick',function(e){
    $.index.toggleLeftView({animated:false}); //animated option only work on ios
    
    
    switch(e.itemId){
      case 'tabletime':
      case 'news':
      case 'contacts':  
      case 'shop':
      case 'activities':
      	$.index.openWindow(Alloy.createController(e.itemId).getView());	
      /*case 'cry':
        //$.index.openWindow(Alloy.createController(e.itemId).getView());
      break;*/ 
      default:
        //$.index.setCenterView(Alloy.createController(e.itemId).getView()); //Arg shold be View(not window)
      break;
     }
});

/*var vertScrollView = Titanium.UI.createScrollView({
    contentWidth:'auto',
    contentHeight:'auto',
    top:0,
    bottom:0,
    showVerticalScrollIndicator:true,
    showHorizontalScrollIndicator:true,
});
$.index.add(vertScrollView);*/

$.index.open();
