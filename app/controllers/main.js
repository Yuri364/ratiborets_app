var args = arguments[0] || {};

/* Расписание */
function schedule(e) {
  var sche_v = Alloy.createController('tabletime',args);
  sche_v.getView().open();
}
var font_family = 'pfdintextcondpro-regular';

function share() {
	
	var tableData = [];
	
	var soc =  [['Вконтакте','vk'],['Facebook','fb']];
	var socIcon = ['\uf107','\uf102'];
	
	var share_win = Ti.UI.createWindow({
	  title: 'Поделиться',
	  //exitOnClose: false,
	  modal:true,
	});
	
	for (var i = 0; i < soc.length; i++) {
		var row = Ti.UI.createTableViewRow({
			layout:'vertical',
			height:"auto",
		});
		
		var view = Ti.UI.createView({
			top: 5,
			bottom: 5,
			height:"auto",
			layout:'vertical',
			rowIndex:soc[i][1],
		});
		
		var labelIcon = Ti.UI.createLabel({
			color: '#fff',
			left: 15,
			top: 10,
			bottom: 10,
			right: 10,
			font: {
				fontFamily: "Flaticon",
				fontSize: 25,
			},
			text: socIcon[i],
			title: socIcon[i],
			rowIndex:soc[i][1],
		});
		view.add(labelIcon);
		
		var labelText = Ti.UI.createLabel({
			color:'#fff',
			top: -37,
			left: 50,
			bottom: 10,
			font:{fontSize:22,fontFamily:font_family},
			text: soc[i][0],
			rowIndex:soc[i][1],
			height:"auto"
		});
		view.add(labelText);
		
		row.add(view);
		tableData.push(row);
	}
	
	var tableView = Ti.UI.createTableView({
		data:tableData
	});
			
	share_win.add(tableView);
	
	share_win.addEventListener("open", function() {
	    if (Ti.Platform.osname === "android") {
	        if (! share_win.activity) {
	            Ti.API.error("Can't access action bar on a lightweight window.");
	        } else {
	            actionBar = share_win.activity.actionBar;
	            if (actionBar) {
	                actionBar.displayHomeAsUp = true;
                    actionBar.onHomeIconItemSelected = function() {
                    	var options = {};
                    	options.activityEnterAnimation = Ti.Android.R.anim.slide_in_left;
        				options.activityExitAnimation = Ti.Android.R.anim.slide_out_right;
                        share_win.close(options);
                    };
	            }
	        }
	    }
	});
	
	share_win.open();
	
	tableView.addEventListener('click', function(e){
		var rowIndex = e.source.rowIndex;
		if(rowIndex == 'vk') {
			var vkontakte = require('ti.vkontakte');
			vkontakte.appid = '5327977';
			vkontakte.permissions = [vkontakte.PERMISSION.POST_WALL, vkontakte.PERMISSION.FRIENDS];
			vkontakte.addEventListener('login', function(e) {
			    if(e.success) {
			        var msg = 'Мобильное приложение фитнес-клуба Ратиборец!'+'\n';
			        	msg += 'Скачать в Google Play: ';
			            msg += 'https://play.google.com/store/apps/details?id=ru.app4biz.ratiborets';
			        vkontakte.makeAPICall('wall.post', {
			            message: msg,
			        });
			    } else if(e.cancelled) {
			        // user pressed cancel button
					//alert('User pressed cancel button');
			    } else {
			        //alert(e.error);
			    }
			});
			
			vkontakte.addEventListener('error', function(e) {
			    alert(e.error);
			});
			
			vkontakte.addEventListener('result', function(e) {
			    if(e.result.hasOwnProperty('post_id')) {
			        Ti.API.info('New post id: '+e.result.post_id);
			        if(e.result.post_id) {
			        	alert('Сообщение было размещено на Вашей стене.');
			        }
					//alert('New message id: '+e.result.post_id);
			    }
			});
			
			vkontakte.authorize();
		}
		else if(rowIndex == 'fb') {
			alert('Данная функция временно недоступна');
		}
		
	});
}

/* Соц. сети */
function soc(e) {
	var viewId = e.source.viewId;
	if(viewId == 1) {
		Titanium.Platform.openURL('http://vk.com/ratiborets_ekb');
	}
	else if(viewId == 2) {
		Titanium.Platform.openURL('https://instagram.com/ratiborets');
	}
	else if(viewId == 3) {
		Titanium.Platform.openURL('https://www.facebook.com/ratiborets.ekb');
	}
	else if(viewId == 4) {
		Titanium.Platform.openURL('https://plus.google.com/+%D0%A0%D0%B0%D1%82%D0%B8%D0%B1%D0%BE%D1%80%D0%B5%D1%86%D0%95%D0%BA%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%BD%D0%B1%D1%83%D1%80%D0%B3/posts');
	}
} 

/* Cтраницы */
function doClick(e) {
    
    var page = Array;
    page[1] = '14wVNSRoDN';
    page[2] = 'L09wmbO7Kf';
    page[3] = 'NkZIC1b9w6';
    page[4] = 'OsrvGC2XsT';
    
    var title = Array;
    title[1] = 'Тренажерный зал';
    title[2] = 'Кроссфит';
    title[3] = 'Единоборства';
    title[4] = 'О клубе';
    
    var args = Array;
    args['id'] = page[e.source.pageID];
    args['title'] = title[e.source.pageID];
    args['page_id'] = e.source.pageID;
    
	var page_v = Alloy.createController('page',args);
	page_v.getView().open();
    
}