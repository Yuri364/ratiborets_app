var args = arguments[0] || {};

var lib = require('global');
var Parse = require('/Parse');

var win = $.news;

var title = "Новости";
lib.action_bar(win,title);

var font_family = 'pfdintextcondpro-regular'; //fontFamily

Parse.getObjects('news?order=-date&where={"public":"1"}', function(res,data,status) {
	
	if(res == 1) {
		
		var tableData = [];
		
		//console.log(data);
		
		data = JSON.parse(data);
		//data = data.results;
		
		var posts = data.results;
		var _post = Array;
		
		if(posts.length>0) {
			//console.log(posts.length);
			for (var i = 0; i < posts.length; i++) {
				var post = posts[i];
				_post[post.objectId] = posts[i];
				//console.log(posts[i]);
				
				var row = Ti.UI.createTableViewRow({
					selectedBackgroundColor:'white',
					rowIndex:post.objectId,
					backgroundColor: '#fff',
					layout:'vertical',
					height: 120,
				});
				
				var imageView = Ti.UI.createImageView({
					left:10, 
					top:10,
					height: 100,
					width: 100,
					bottom: 10,
					image: post.photo.url,
					rowIndex: post.objectId,
				});
				row.add(imageView);
				
				var labelTitle = Ti.UI.createLabel({
					color:'#2a2a2a',
					font:{fontSize:22,fontFamily:font_family},
					text: post.title,
					left:120, 
					top: -100,
					right: 5,
					rowIndex:post.objectId,
					height:"auto"
				});
				row.add(labelTitle);
				
				var labelCalendar = Ti.UI.createLabel({
					color:'#f00',
					font:{
						fontSize:15,
						fontFamily: "Ionicons",
					},
					text: "\uf117",
					left:120, 
					top: 5,
					rowIndex:post.objectId,
					height:"auto"
				});
				row.add(labelCalendar);
				
				var labelDate = Ti.UI.createLabel({
					text: lib.normalDate(post.date,0),
					color: '#f00',
					font:{fontSize:15,fontFamily:font_family},
					left:137, 
					top: -17,
					rowIndex:post.objectId,
				});
				row.add(labelDate);
				
				/*var labelDesc = Ti.UI.createLabel({
					color:'#2a2a2a',
					font:{fontSize:12},
					text: lib.cutLongString(post.text,80),
					left:120, 
					top: -5, 
					right: 10,
					bottom: 15,
					rowIndex:post.objectId,
				});
				row.add(labelDesc);*/
				
				tableData.push(row);
			}
		} //else 0
		
		var tableView = Ti.UI.createTableView({
			backgroundColor:'white',
			separatorColor: '#f1f1f1',
			//left: 3, right: 3,top: 3, bottom: 3,
			data:tableData
			//separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,//ios
		});
			
		win.add(tableView);
		
		tableView.addEventListener('click', function(e){
			var objectId = e.source.rowIndex;
			//console.log(e.source);
			full_post(_post[objectId]);
		});
		
	}
	
});

//Полная новость
function full_post(post) {
	var args =  post;
	var full_post = Alloy.createController('full_post',args);
	full_post.getView().open();
}
