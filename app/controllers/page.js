var args = arguments[0] || {};

var lib = require('global');
var Parse = require('/Parse');

var win = $.page;

var title = args.title;
lib.action_bar(win,title);

Parse.getObject('page', args.id, function(res,data,status) {
	
	if(res == 1) {
		
		data = JSON.parse(data);
		data = data.results[args.page_id-1];
		
		var scrollView = Ti.UI.createScrollView({
		  showVerticalScrollIndicator: true,
		  showHorizontalScrollIndicator: true,
		  height: 'auto',
		  width: 'auto'
		});
		
		var viewBg = Titanium.UI.createView({
		   top: 0,
		   left: 0,
		   right: 0,
		   bottom: 0,
		   height: "auto",
		   widht: "auto",
		   backgroundColor: "#fff",
	   });	
		
		var labelText = Ti.UI.createLabel({
				color:'#2a2a2a',
				font:{
					fontSize: 19,
					fontFamily: "pfdintextcondpro-regular",
				},
				text: data.text,
				left:10, 
				top: 10,
				right: 10,
				height:"auto"
		});
		
		viewBg.add(labelText);
		scrollView.add(viewBg);
		win.add(scrollView);
	}
});