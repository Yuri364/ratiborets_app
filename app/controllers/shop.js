var args = arguments[0] || {};

var lib = require('global');
var Parse = require('/Parse');

var win = $.shop;

var title = "Магазин Ратиборец";
lib.action_bar(win,title);

Parse.getObjects('shop?order=createdAt', function(res,data,status) {
	if(res == 1) {
		
		var tableData = [];
		
		//console.log(data);
		
		data = JSON.parse(data);
		data = data.results;
		//goods
		var goods = data;
		for (var i = 0; i < goods.length; i++) {
			var good = goods[i];
			
			console.log(good);
			
			var row = Ti.UI.createTableViewRow({
				selectedBackgroundColor:'white',
				rowIndex: good.objectId,
				backgroundColor: '#fff',
				//layout:'vertical',
				height:"auto",
			});
			
			var imageView = Ti.UI.createView({
				left:10, 
				top:10,
				width: 160,
				height: 145,
				backgroundColor: "#ccc",
				backgroundImage: good.img.url,
				//borderRadius:5, //For ios
				rowIndex:goods.objectId,
			});
			row.add(imageView);
			
			var labelTitle = Ti.UI.createLabel({
				color:'red',
				font:{fontSize:14},
				text: good.title,
				left:175, 
				top: 10,
				//left: 5,
				right: 5,
				rowIndex:good.objectId,
				height:"auto"
			});
			row.add(labelTitle);
			
			tableData.push(row);
			
		}
		
		var tableView = Ti.UI.createTableView({
			backgroundColor:'white',
			separatorColor: '#f1f1f1',
			//left: 3, right: 3,top: 3, bottom: 3,
			data:tableData
			//separatorStyle: Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,//ios
		});	
		
		win.add(tableView);
		
	}
});