var args = arguments[0] || {};

var win = $.tabletime;
var title = "Расписание";

//win.id = 'red';

var Parse = require('/Parse');

var lib = require('global');
lib.action_bar(win,title);

var days = ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];

var marginLabelLeft = 0;
var viewDayWidth = 100/7;

var days_bg = '#3b3b3b';
var view_bg = 'white';
var date = new Date();
var nowDay = date.getDay();
var day = 1;

var font_family = 'pfdintextcondpro-regular'; //fontFamily

var viewDays = Titanium.UI.createView({
	   top: 0,
	   left: 0,
	   height: 50,
	   backgroundColor: days_bg,
	   width: 'auto'
});

now_day = nowDay==0?7:nowDay;

for (var i = 0; i < days.length; i++) {
	    
	   date = new Date();
	   //Считаем с понедельника
	   date.setDate(date.getDate()-now_day+1);
	   date.setDate(date.getDate()+i);
	   var now_date = date.getDate();
	   
	   //console.log(nowDay);
	   
	   var viewDay = Titanium.UI.createView({
		   top: 0,
		   left: viewDayWidth*i+'%',
		   height: 50,
		   backgroundColor: nowDay==day?view_bg:days_bg,
		   width: viewDayWidth+'%',
		   viewId: i+1,
		   touchEnabled: true
	   });
	   
	   var labelDay = Titanium.UI.createLabel({
	   	   top: 5,
	   	   font:{fontSize:16,fontFamily:font_family},
		   text: days[i],
		   textAlign: 'center',
		   color: nowDay==day?days_bg:view_bg,
		   labelDayId: i,
		   touchEnabled: false
	   });
	   
	   var labelWeekDay = Titanium.UI.createLabel({
	   	   top: 25,
	   	   font:{fontSize:18,fontFamily:font_family},
		   text: now_date,
		   textAlign: 'center',
		   color: nowDay==day?days_bg:view_bg,
		   labelWeekDayId: i,
		   touchEnabled: false
	   });
	   
	   viewDay.add(labelWeekDay);
	   viewDay.add(labelDay);
	   viewDays.add(viewDay);
	   
	   day = i==5?0:day+1;
}

win.add(viewDays);

function show_tabletime(day) {
	Parse.getObjects('schedule', schedule); //Все программы
	
	var tableData = [];
					
	var tableView = Ti.UI.createTableView({
		backgroundColor:'white',
		top: 50,
		separatorColor: '#f1f1f1',
		layout:'vertical',
	});
	
	tableView.setData([]);
	
	function schedule(res,data,status) {
		if(res == 1) {
			data = JSON.parse(data);
			s_data = data.results;
			Parse.getObjects('tabletime', function tableTime(res,data,status) {
				if(res == 1) {
					data = JSON.parse(data);
					t_data = data.results;
					
					for (var i = 0; i < t_data.length; i++) {
						if(day == t_data[i].day) {
							for (var l = 0; l < t_data[i].schedule.length; l++) {
								
								var time = t_data[i].schedule[l][0];
								var sid = t_data[i].schedule[l][1];
								
								var group = t_data[i].schedule[l][2];
								var place = t_data[i].schedule[l][3];
								var duration = t_data[i].schedule[l][4];
								
								for (var i1 = 0; i1 < s_data.length; i1++) {
									//objectId
									if(s_data[i1].objectId == sid) {
										var row = Ti.UI.createTableViewRow({
											backgroundColor: '#fff',
											layout:'vertical',
											height:"auto",
										});
										var labelTime = Ti.UI.createLabel({
											color:'red',
											font:{fontSize:28,fontFamily:font_family},
											text: time,
											left:10,
											right: 10,
											top: 5,
											height:"auto"
										});
										row.add(labelTime);
										
										var labelsView = Ti.UI.createView({
											left:65,
											top:-27,
											height:"auto",
											layout:'vertical',
										});
										
										var labelTitle = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:23,fontFamily:font_family},
											text: s_data[i1].title,
											left:10,
											right: 10,
											top: -5,
											height:"auto"
										});
										labelsView.add(labelTitle);
										
										//group
										var labelGroup = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:20,fontFamily:font_family},
											text: group,
											left:10,
											right: 10,
											bottom: 5,
											height:"auto"
										});
										labelsView.add(labelGroup);
										
										var labelFlagIcon = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:16,fontFamily: "Ionicons"},
											text: "\uf279",
											left:10,
											height:"auto"
										});
										labelsView.add(labelFlagIcon);
										
										var labelPlace = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:16},
											text: place,
											left:30,
											right: 10,
											top: -20,
											height:"auto"
										});
										labelsView.add(labelPlace);
										
										var labelTimeIcon = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:16,fontFamily: "Ionicons"},
											text: "\uf26e",
											left:10,
											height:"auto"
										});
										labelsView.add(labelTimeIcon);
										
										var labelDuration = Ti.UI.createLabel({
											color:'#2a2a2a',
											font:{fontSize:22,fontFamily:font_family},
											text: duration+' мин',
											left:30,
											right: 10,
											top: -20,
											bottom: 5,
											height:"auto"
										});
										labelsView.add(labelDuration);
										row.add(labelsView);
										
										tableData.push(row);
										
										
									}
								}
							}
						}
					}
					
					tableView.setData(tableData);
					win.add(tableView);
				}
			});

			
		}
	}
	
}

viewDays.addEventListener('click', function(e){ 
	
	 if(win.children[2] != undefined) {
	 	win.remove(win.children[2]);//remove table
	 } 
	
	var viewId = e.source.viewId;
	
	e.source.applyProperties({
        backgroundColor: view_bg
    });
	e.source.children[0].applyProperties({
        color: days_bg
    });
    e.source.children[1].applyProperties({
        color: days_bg
    });
    
    show_tabletime(viewId);
    
    var childrenView = viewDays.children;
    
    for (var i = 0; i < childrenView.length; i++) {
    	var childViewId = childrenView[i].viewId;
    	
    	if(childViewId != viewId) {
    		childrenView[i].applyProperties({
		        backgroundColor: days_bg
		    });
    		childrenView[i].children[0].applyProperties({
		        color: view_bg
		    });
		    childrenView[i].children[1].applyProperties({
		        color: view_bg
		    });
    	}
    }
});

show_tabletime(now_day);

win.addEventListener('swipe', function(e){
	//alert('swipe');
});