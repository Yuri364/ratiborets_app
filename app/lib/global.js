
//Action bar
exports.action_bar = function(win,title) {
	
	//ActionBar android
	win.addEventListener("open", function() {
	    if (Ti.Platform.osname === "android") {
	    	
	        if (! win.activity) {
	            //Ti.API.error("Can't access action bar on a lightweight window.");
	        } else {
	            actionBar = win.activity.actionBar;
	            if (actionBar) {
	                actionBar.backgroundImage = "/top_bg.png";
	                actionBar.title = title;
	                actionBar.displayHomeAsUp = true;
                    actionBar.onHomeIconItemSelected = function() {
                    	var options = {};
                    	options.activityEnterAnimation = Ti.Android.R.anim.slide_in_left;
        				options.activityExitAnimation = Ti.Android.R.anim.slide_out_right;
                        win.close(options);
                    };
	            }
	        }
	    }
	    else {
	    	win.backButtonTitle = title;
	    	win.barColor = 'red';
	    	win.navTintColor = '#fff';
	    }
	});
};

exports.cutLongString = function(str, count_lit) {
    var text = str;
    var all_len = text.length;
    var new_text;
    if (all_len > count_lit){
        // обрезаем текст и добавляем три точки в конец
        new_text = text.substr(0, (count_lit - 3)) + '...';    
        return new_text;
    }
    return text;
};

exports.normalDate = function(strDate,t) {
	var date = strDate.iso.split("T");
	_date = date[0].split("-");
	var months = {};
	months['01'] = 'Января';
	months['02'] = 'Февраля';
	months['03'] = 'Марта';
	months['04'] = 'Апреля';
	months['05'] = 'Мая';
	months['06'] = 'Июня';
	months['07'] = 'Июля';
	months['08'] = 'Августа';
	months['09'] = 'Сентября';
	months['10'] = 'Октября';
	months['11'] = 'Ноября';
	months['12'] = 'Декабря';
	if(t) {
		var time = date[1].split(":");
		time = time[0]+':'+time[1];
		var d = [];
		d['date'] = _date[2]+' '+months[_date[1]]+' '+_date[0];
		d['time'] = time;
		return d;
	}
	return _date[2]+' '+months[_date[1]]+' '+_date[0];
};

//Debug
exports.alertObj = function (obj) {
    var str = ""; 
    for(k in obj) { 
        str += k+": "+ obj[k]+"\r\n"; 
    } 
    alert(str); 
};
